import openpyxl
import smtplib
# -*- coding: utf-8 -*-
excel_document = openpyxl.load_workbook('sample.xlsx')
sheet = excel_document.get_sheet_by_name('Playera')

def createMessage(email, direccion, nombre, talla):
  fichero = open ( email+'.txt', 'w' ) 
  fichero.write("Hola estamos a nada de enviarte la playera, te agradecemos infinitamente por apoyar y sumarte a la causa para preparar el envio necesitamos que nos confirmes algunos datos: \nNombre: {nombre} \nTalla: {talla} \nDireccion: {direccion} \nNOTA:(te agradeceremos si puedes detallar tu direccion, si ya has recibido paquetes a esta direccion entonces has caso omiso a este mensaje). \nCualquier duda quedo atento :)".format(nombre=nombre,talla=talla, direccion=direccion))
  fichero.close()
  # mensaje = "Hola estamos a nada de enviarte la playera, te agradecemos infinitamente por apoyar y sumarte a la causa para preparar el envio necesitamos que nos confirmes algunos datos: \nNombre: {nombre} \nTalla: {talla} \nDireccion: {direccion} \nNOTA:(te agradeceremos si puedes detallar tu direccion, si ya has recibido paquetes a esta direccion entonces has caso omiso a este mensaje). \nCualquier duda quedo atento :)".format(nombre=nombre,talla=talla, direccion=direccion) 
  # sendMessage(mensaje)
  return "Hola estamos a nada de enviarte la playera, te agradecemos infinitamente por apoyar y sumarte a la causa para preparar el envio necesitamos que nos confirmes algunos datos: \nNombre: {nombre} \nTalla: {talla} \nDireccion: {direccion} \nNOTA:(te agradeceremos si puedes detallar tu direccion, si ya has recibido paquetes a esta direccion entonces has caso omiso a este mensaje). \nCualquier duda quedo atento :)".format(nombre=nombre,talla=talla, direccion=direccion)


for row in sheet.iter_rows(min_row=2):
  email, direccion, nombre, talla = row
  mensaje = createMessage(email.value.encode('utf-8'), direccion.value.encode('utf-8'), nombre.value.encode('utf-8'), talla.value.encode('utf-8'))
  print(mensaje)

#   #servidor de correos
# def sendMessage(mensaje):
#   remitente = "petriz@codigofacilito"
#   asunto = "Playera Fuerza Mexico" 
#   mensaje = mensaje 
#   return "email enviado"

